/*
 * PPG Speed Controller Code
 * 
 * 073019
 * Author: AA
 */

#include <avr/io.h>
#include <util/delay.h>
#include <TimerOne.h>
#include <math.h>

/*
 * Variables for stepper motor controller
 */
 
#define stp 7         
#define dir 6
#define MS1 10
#define MS2 11
#define EN  3

/****************************************/

unsigned long loopCounter   =0;
unsigned long openCounterStart   =0;
unsigned long openLoopCounter   =0;


int stepCounter; //do not initialize...
int edge_flag               =0;

//Trim for period measurement, microseconds added to period measurement
//Set at 100 7/29/2019 on during bench testing
int periodOffset      =100;

unsigned long time0              =0; 
unsigned long time1              =0; 
unsigned long elapsed            =0; 
unsigned long elapsed_prev       =0; 
unsigned long On_DC              =0; 
unsigned long Off_DC             =0; 
unsigned long On_DC_prev         =0; 
unsigned long Off_DC_prev        =0; 
unsigned long out                =0; 
unsigned long Sum                =0;
unsigned long first              =0;
unsigned long loop_time          =0;
unsigned long engine_speed       =0;       //11900 usec for 5000rpm


int speedSetPoint       =9230;//10000;
int deadBand            =200; //deadband is period in us plus/minus this value in us
int disableCounter      =0;
int motorDisabled       =0; 
int lowSpeedThreshold   =speedSetPoint + deadBand; 
int highSpeedThreshold  =speedSetPoint - deadBand;
int bigStepCount        =0; //counts how many full steps you took at a time
int speedError          =0; //should range from 50 to 500 or so, 50 being the fastest
int speedError_old      =0;
int D                   =0;
int openFlag=0;  //openFlag =1 indicates that we have in last 500msec done a fullstepopen 

//Change this variable to change frequency of step pulses... 50 ~ 490-500 Hz
int loopCounterThreshold = 100; //50 is 500 Hz to stepper motor, lowest allowable value   

float ADC_VALUE         =0;
 
const byte adcPin   =1;  //A1 Analog pin -- this is reserved for if/when we decide to take TPS input into account... 

bool adcEnabled         =false;
bool serial_flag        =false;
bool motorJustMoved     =false;
bool currentlyStepping  =false;

void elapse(){
  elapsed++;
}
void ON_DutyCycle(){
  On_DC++;
}
void OFF_DutyCycle(){
  Off_DC++;
}


void fullCloseThrottle()
{
  digitalWrite(dir, HIGH); //Pull direction pin low to move "forward"
  for(stepCounter= 1; stepCounter<280; stepCounter++)  //Loop the forward stepping enough times for motion to be visible
  {
    digitalWrite(stp,HIGH); //Trigger one step forward
    delayMicroseconds(1);
    digitalWrite(stp,LOW); //Pull step pin low so it can be triggered again
    delay(5);
  }
}

void fullOpenThrottle()
{
  digitalWrite(dir, LOW); //Pull direction pin low to move "forward"
  for(stepCounter= 1; stepCounter<500; stepCounter++)  //Loop the forward stepping enough times for motion to be visible
  {
    digitalWrite(stp,HIGH); //Trigger one step forward
    delayMicroseconds(1);
    digitalWrite(stp,LOW); //Pull step pin low so it can be triggered again
    delay(5);
  }
}

void StepOpenThrottle(int numSteps)
{
  digitalWrite(dir, LOW); //Pull direction pin low to move "forward"
  for(stepCounter= 1; stepCounter<numSteps; stepCounter++)  //Loop the forward stepping enough times for motion to be visible
  {
    digitalWrite(stp,HIGH); //Trigger one step forward
    delayMicroseconds(1);
    digitalWrite(stp,LOW); //Pull step pin low so it can be triggered again
    delay(5);
  }
}

void closeThrottleStart()
{
  digitalWrite(dir, HIGH); //Pull direction pin low to move "forward"
  for(stepCounter= 1; stepCounter<25; stepCounter++)  //Loop the forward stepping enough times for motion to be visible
  {
    digitalWrite(stp,HIGH); //Trigger one step forward
    delayMicroseconds(1);
    digitalWrite(stp,LOW); //Pull step pin low so it can be triggered again
    delay(5);
  }
}

void resetEDPins()
{
  digitalWrite(stp, LOW);
  digitalWrite(dir, LOW);
  digitalWrite(MS1, LOW);
  digitalWrite(MS2, LOW);
  digitalWrite(EN,  LOW);    // low allows motor control
}

void disableStepper() {
  PORTD = PORTD | B00001000;     //digital pin 3 goes high, others remain unchanged from current state
} 

void enableStepper() {
  PORTD = PORTD & B11110111;     //digital pin 3 goes low, others remain unchanged from current state
}

void closeThrottleMode () {
  PORTD = PORTD | B01000000;     
}

void openThrottleMode () {
  PORTD = PORTD & B10111111;
}

void startCurrentStep () {
  PORTD = PORTD | B10000000;
}

void endCurrentStep () {
  PORTD = PORTD & B01111111;
}

void fullStepMode() {
  PORTB = PORTB & B11110011; 
}

void halfStepMode() {
  PORTB = PORTB | B00000100;
  PORTB = PORTB & B11110111; 
}

void quarterStepMode() {
  PORTB = PORTB & B11111011;
  PORTB = PORTB | B00001000; 
}

void eighthStepMode() {
  PORTB = PORTB | B00001100;
}

/*
 * Setup function sets the pin-modes and opens the throttle fully -- this runs whevever the system is power-cycled / reset
 */
void setup(){
  pinMode(8,    INPUT);
  pinMode(A1,   INPUT);
  pinMode(9,   OUTPUT);
  pinMode(12,  OUTPUT);
  pinMode(stp, OUTPUT);
  pinMode(dir, OUTPUT);
  pinMode(MS1, OUTPUT);
  pinMode(MS2, OUTPUT);
  pinMode(EN,  OUTPUT);

  resetEDPins();
  int tempADC = analogRead(A1); 
  int initialOpen = (int) (0.82 * tempADC) - 91; //reduced from 101 12/10
  if (initialOpen > 90) initialOpen  = 90;
  if (initialOpen < 0)  initialOpen  =  1; 
  
  fullCloseThrottle();
   
  resetEDPins();
  StepOpenThrottle(initialOpen);
  
  delay(15000);
  StepOpenThrottle(5);
  delay(3000);
  StepOpenThrottle(5);
  delay(2000);
  StepOpenThrottle(5);
  delay(1000);
  StepOpenThrottle(5);
  delay(1000);
  //StepOpenThrottle(5);
  //delay(1000); 
}


void loop() {

  loopCounter++;
  openLoopCounter++;
  

  if (loopCounter>loopCounterThreshold && currentlyStepping == false) 
  {
    if (openFlag==0){
      speedSetPoint=10000;
    }
    engine_speed = engine_speed / loopCounter / 2; //divide by two to account for step pulse up time and down time
    speedError = engine_speed - speedSetPoint;
    speedError = abs(speedError);
    if (openFlag==1 && openLoopCounter>openCounterStart+75000){
      openFlag=0;
      openLoopCounter=0;
    }
    
    if (speedError > 2500)  {
      //if (bigStepCount < 20) {
        //fullStepMode();
        
        if (engine_speed > lowSpeedThreshold && openFlag==0){
       //if (openFlag==0){
          quarterStepMode();
          
          
            openCounterStart=openLoopCounter;
            openFlag=1;
           // speedSetPoint=11500;
          
          //speedSetPoint=9000;
        
       
     //   }
        //bigStepCount++;
      } else {
        //speedSetPoint=10000;
        eighthStepMode();
      } 
//      else{
//         eighthStepMode();
//    }
    /* 
    else if (speedError > 2750) {
      halfStepMode();
    } 
    else if (speedError > 750) {
      quarterStepMode();
    } 
    else if (speedError > 300) {
      eighthStepMode();
    } 
    else if (speedError > 200) {
      eighthStepMode();
    }
    */ 
  }else if (speedError > 200 && openFlag==0) {
      eighthStepMode();
      bigStepCount = 0; 
    //  speedSetPoint=10000;
      
    }
    else {
      disableStepper();
      motorDisabled = 1;
      bigStepCount = 0; 
     // speedSetPoint=10000;
      
    }

    if (engine_speed > lowSpeedThreshold) 
    {
      openThrottleMode();
      enableStepper();
    } 
    else if (engine_speed < highSpeedThreshold && openFlag==0)
    {
      closeThrottleMode(); 
      enableStepper();
    }
    else
    {
      disableStepper();
    }
   
    engine_speed = 0;
    startCurrentStep();
    currentlyStepping = true;
    loopCounter =0; 
  }   
  else if (loopCounter>loopCounterThreshold && currentlyStepping == true) 
  {
    endCurrentStep();
    currentlyStepping = false;
    loopCounter =0; 
  }
  
  
  time0=micros();                        //note there is lag in recycling the loop 
  loop_time=time0-time1;                 //loop time
  out=out+loop_time;                     //total time until reset by Servo output every 20 ms
  
  PORTB |=(1<< PORTB4);                  //turn on D12 to track loop time 070119 This is not adcEnabled, pin 12 does not track loop time, it just stays high...

  //Reading analog pin for scaling input, without slowing the code, ADC will return a flag when reading is ready  
  if (!adcEnabled) //If the ADC converter is not doing anything, we start taking a reading
  {
    bitSet (ADCSRA, ADSC);             //start a conversion
    adcEnabled = true;
  }
    
  if (bit_is_clear(ADCSRA, ADSC)) //the ADC clears the bit when done
  {
    ADC_VALUE = ADC;  //read result
    adcEnabled = false;  //After we have read the result we set the bit to false because the ADC is turned off again
  }

  //EDGE detection and period measurements
  // start count if D8 goes high


  /*
  reading port B, if pin B0 (D8) is high then turn PinB5 (D13 High)
  In other words, we are checking to see if Pin 8 is high so we can measure the on
  time in order to get the frequency --- it seems like D13 is just an "output" of the input frequency 
  That we are reading...
  */
  if((PINB & (1<<PB0)) >0)
  {
                                    
    if (edge_flag ==0) //write some comments to help the understanding here... 
    {
      elapsed_prev    =elapsed;                 
      On_DC_prev      =On_DC;
      Off_DC_prev     =Off_DC;
      On_DC           =0;
      Off_DC          =0;
      elapsed         =0;          //first time in the loop, rising edge detected
      edge_flag       =1;
     }
     
     PORTB |=(1<< PORTB5);                 //make pin 13 high and power on the led
     elapse();                             //loop counter for on time 
     ON_DutyCycle();   
  } 
  
  //stop count when D9 goes low
  if((PINB & (1<<PB0)) ==0)
  {                                  //reading port B //if pin B0 (D8) is high then turn PinB5 (D13 High)
    edge_flag = 0;                   //reset for next time edge goes high 
    PORTB &= ~(1<<PORTB5);           //make pin 13 low and power off the led
    elapse();                        //continue loop counter to count entire period
    OFF_DutyCycle();
  }

  engine_speed += (elapsed_prev * loop_time) + periodOffset;
  
  PORTB &= ~(1<<PORTB4); //D12 low to track loop time
  time1=time0;
  
}
