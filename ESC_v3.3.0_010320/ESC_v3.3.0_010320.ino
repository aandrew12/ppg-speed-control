/*
 * PPG Speed Controller Code
 * 
 * 073019
 * Author: AA
 */

#include <avr/io.h>
#include <util/delay.h>
#include <TimerOne.h>
#include <math.h>

/*
 * Variables for stepper motor controller
 */
 
#define stp 7         
#define dir 6
#define MS1 10
#define MS2 11
#define EN  3

/****************************************/

unsigned long loopCounter   =0;
int stepCounter; //do not initialize...
int edge_flag               =0;

//Trim for period measurement, microseconds added to period measurement
//Set at 100 7/29/2019 on during bench testing
int periodOffset      =100;

unsigned long time0              =0; 
unsigned long time1              =0; 
unsigned long elapsed            =0; 
unsigned long elapsed_prev       =0; 
unsigned long On_DC              =0; 
unsigned long Off_DC             =0; 
unsigned long On_DC_prev         =0; 
unsigned long Off_DC_prev        =0; 
unsigned long out                =0; 
unsigned long Sum                =0;
unsigned long first              =0;
unsigned long loop_time          =0;
unsigned long engine_speed       =0;        //period of the spark frequency in microseconds, 1 spark / revolution   


int speedSetPoint       =9230;  //11500;  //9230; //9230=6500, 10720 ~ 5600 RPM
int speedError          =0; 
int stepperDisabled     =0; 
int disabledCounter     =0;
int runAwayCounter      =0;
int runAwayFlag         =0; 
int swungClosed         =0;
int slowOpenCounter     =0;
int swungOpen           =0;
int slowCloseCounter    =0;

//Change this variable to change frequency of step pulses... 50 ~ 490-500 Hz
int loopCounterThreshold = 50; //50 is 500 Hz to stepper motor, lowest allowable value   

float ADC_VALUE         =0;
 
const byte adcPin   =1;  //A1 Analog pin -- this is reserved for if/when we decide to take TPS input into account... 

bool adcEnabled         =false;
bool serial_flag        =false;
bool motorJustMoved     =false;
bool currentlyStepping  =false;

void elapse(){
  elapsed++;
}
void ON_DutyCycle(){
  On_DC++;
}
void OFF_DutyCycle(){
  Off_DC++;
}


void fullCloseThrottle()
{
  digitalWrite(dir, HIGH); //Pull direction pin low to move "forward"
  for(stepCounter= 1; stepCounter<280; stepCounter++)  //Loop the forward stepping enough times for motion to be visible
  {
    digitalWrite(stp,HIGH); //Trigger one step forward
    delayMicroseconds(1);
    digitalWrite(stp,LOW); //Pull step pin low so it can be triggered again
    delay(5);
  }
}

void fullOpenThrottle()
{
  digitalWrite(dir, LOW); //Pull direction pin low to move "forward"
  for(stepCounter= 1; stepCounter<500; stepCounter++)  //Loop the forward stepping enough times for motion to be visible
  {
    digitalWrite(stp,HIGH); //Trigger one step forward
    delayMicroseconds(1);
    digitalWrite(stp,LOW); //Pull step pin low so it can be triggered again
    delay(5);
  }
}

void StepOpenThrottle(int numSteps)
{
  digitalWrite(dir, LOW); //Pull direction pin low to move "forward"
  for(stepCounter= 1; stepCounter<numSteps; stepCounter++)  //Loop the forward stepping enough times for motion to be visible
  {
    digitalWrite(stp,HIGH); //Trigger one step forward
    delayMicroseconds(1);
    digitalWrite(stp,LOW); //Pull step pin low so it can be triggered again
    delay(5);
  }
}

void closeThrottleStart()
{
  digitalWrite(dir, HIGH); //Pull direction pin low to move "forward"
  for(stepCounter= 1; stepCounter<25; stepCounter++)  //Loop the forward stepping enough times for motion to be visible
  {
    digitalWrite(stp,HIGH); //Trigger one step forward
    delayMicroseconds(1);
    digitalWrite(stp,LOW); //Pull step pin low so it can be triggered again
    delay(5);
  }
}

void resetEDPins()
{
  digitalWrite(stp, LOW);
  digitalWrite(dir, LOW);
  digitalWrite(MS1, LOW);
  digitalWrite(MS2, LOW);
  digitalWrite(EN,  LOW);    // low allows motor control
}

void disableStepper() {
  PORTD = PORTD | B00001000;     //digital pin 3 goes high, others remain unchanged from current state
} 

void enableStepper() {
  PORTD = PORTD & B11110111;     //digital pin 3 goes low, others remain unchanged from current state
}

void closeThrottleMode () {
  PORTD = PORTD | B01000000;     
}

void openThrottleMode () {
  PORTD = PORTD & B10111111;
}

void startCurrentStep () {
  PORTD = PORTD | B10000000;
}

void endCurrentStep () {
  PORTD = PORTD & B01111111;
}

void fullStepMode() {
  PORTB = PORTB & B11110011; 
}

void halfStepMode() {
  PORTB = PORTB | B00000100;
  PORTB = PORTB & B11110111; 
}

void quarterStepMode() {
  PORTB = PORTB & B11111011;
  PORTB = PORTB | B00001000; 
}

void eighthStepMode() {
  PORTB = PORTB | B00001100;
}

/*
 * Setup function sets the pin-modes and opens the throttle fully -- this runs whevever the system is power-cycled / reset
 */
void setup(){  
  
  pinMode(8,    INPUT);  //frequency input (reluctance sensor GEN board)
  pinMode(A1,   INPUT);  //block temp sensor input
  pinMode(9,   OUTPUT);  //troubleshooting
  pinMode(12,  OUTPUT);  //troubleshooting
  pinMode(stp, OUTPUT);  //motor control
  pinMode(dir, OUTPUT);  //motor control
  pinMode(MS1, OUTPUT);  //motor control
  pinMode(MS2, OUTPUT);  //motor control
  pinMode(EN,  OUTPUT);  //motor control

  resetEDPins();                           //reset control pins on stepper motor driver
  fullCloseThrottle();                     //close throttle to begin startup routine....

  int tempADC = analogRead(A1);            //read initial block temp -- use this to set startup schedule...

  int cold_steps =   60;                    //number of steps to take during cold (RT) start (less than RT will always be 10 steps)                    
  int cold_adc   =  147;                    //corresponding cold (RT) start temperature: 147 ADC ~ 22C  
  
  int hot_steps  =  100;                     //number of steps to take during hottest start (block at controlled temp.) 
  int hot_adc    =  287;                    //corresponding hot start temperature: 287 ADC ~ 90C

  int initialOpen = map(                   //interpolate the temperature value that we just read into a number of steps
    tempADC,                               //initial block temp       
    cold_adc, hot_adc,                     //adc bounds of block temperature
    cold_steps, hot_steps                  //number of steps to take at upper and lower temperature bounds
  );

  constrain(                               //assure that initialOpen falls within upper and lower bounds
    initialOpen, 
    cold_steps, hot_steps
  );
  
  resetEDPins();                           //reset control pins on stepper motor driver
  
  delay(250);                              //wait a moment once the stepper hits idle screw
  StepOpenThrottle(1);                     //move a step to relieve pressure on idle screw
  delay(250);                              //wait a moment before initial open
  
  StepOpenThrottle(initialOpen);           //open throttle initially for pull start using value calculated above
  
  double delay_multiplier = map(          //interpolate delay multiplier -- should be twice as fast at hottest temp.
    tempADC,                              //intial block temp
    cold_adc, hot_adc,                    //adc bounds of temperature
    100, 60);                             //multiplier bounds (100 at room temp, 50 at hot start) --> 1.0 and 0.5

  delay_multiplier = (double) delay_multiplier / 100;    //divide by 100 to get real multiplier (map function above uses integers only)

  constrain(                                           //constrain multipler within upper and lower bounds 
    delay_multiplier,
    0.6, 1.0
  );
  
  int delay_array[]  = {10000, 10000, 5000, 4000, 4000, 800, 600, 600};   //delays in startup routine in microseconds
  int step_array[]   = {10,    5,     5,    5,    5,    5,   5,   5  };   //number of steps after each delay in startup routine
  
  for (int j=0; j<8; j++) {                                       //startup routine: slowly speed up engine before entering speed control mode
    delay_array[j] = delay_multiplier * delay_array[j];           //multiply the delay
    if (j==0) {
      delay(10000);                                               //do not use multipler on first delay
    } else {
      delay(delay_array[j]);                                      //use multiplier on all other delays... 
    }
    StepOpenThrottle(step_array[j]);                              //execute stepping --> repeat for every array element
  }

} //end of startup sequence, move into speed control mode...


void loop() {

  loopCounter++;
  

  if (loopCounter>loopCounterThreshold && currentlyStepping == false) 
  {
    
    engine_speed = engine_speed / loopCounter / 2; //divide by two to account for step pulse up time and down time
    speedError = engine_speed - speedSetPoint;
    speedError = abs(speedError);
    resetEDPins();

    eighthStepMode();                    //unsure if this line is needed

    /*Engine speed too slow, open throttle*/
    if (engine_speed > speedSetPoint + 250)            //if engine period is higher than the setpoint, engine is slow, open the throttle
    {
      swungOpen = 0;                             //since speed is low we know we didn't just swing open
      openThrottleMode();
      runAwayFlag = 0;                          //reset the runaway flag/counter, we know the generator is not running too fast
      runAwayCounter = 0;                     
      loopCounterThreshold = 50;  

      if (speedError > 2000) {           //if there is a large speed error, swing open
        swungOpen = 1;                   //set the swung open flag because we just reacted quickly
        halfStepMode();
        enableStepper();
        loopCounterThreshold = 50;
      } 
      else {                             //if there error is not large, open slowly
        eighthStepMode();  
        loopCounterThreshold = 100;
        enableStepper();
      }
      
    }
    /*Engine speed too fast, close the throttle*/ 
    else if (engine_speed < speedSetPoint - 250)    //else if the period is lower than the setpoint - deadband, close the throttle
    {

      closeThrottleMode();
      
      if (speedError > 2000) {                       //if there is a large error, we still close slowly, runaway is captured elsewhere
        eighthStepMode(); 
        loopCounterThreshold = 150;
        enableStepper();
      }
      else {
        eighthStepMode();                            //catch all -- close slowly TODO: combine this and above into one
        loopCounterThreshold = 150;  
        enableStepper();
      }
    }
    /*engine speed within deadband -- disable motor*/
    else
    {
      disableStepper();                                   //else disable the stepper and set a flag to know we found a good spot
      stepperDisabled = 1;
    }

    /*keep motor disabled for a few counts if it caught the deadband*/
    if (stepperDisabled == 1) {                            //if the stepper was recently disabled, keep it disabled for a number of counts
      disableStepper();
      disabledCounter++;
        if (disabledCounter > 75) {                       //adjustment for how long the motor stays disabled 
          disabledCounter = 0;
          stepperDisabled = 0;
        }
    }

    if (engine_speed < 7800) {                         //if engine is running very fast (possible brick shut down) set a flag
      runAwayFlag = 1;
      runAwayCounter++;                                //count how many times we saw this high speed               
        if (runAwayCounter > 30) {                     //if we see very high speed for a number of counts, swing closed
          closeThrottleMode();
          halfStepMode();
          enableStepper();
          loopCounterThreshold = 50;
          swungClosed = 1;                              //set a flag to know that we just swung closed
        }
    }
   
    engine_speed = 0;                                   //reset variables get ready to step again....
    startCurrentStep();
    currentlyStepping = true;
    loopCounter =0; 
  }   
  
  else if (loopCounter>loopCounterThreshold && currentlyStepping == true) 
  {
    endCurrentStep();
    currentlyStepping = false;
    loopCounter =0; 
  }
  
  time0=micros();                        //note there is lag in recycling the loop 
  loop_time=time0-time1;                 //loop time
  out=out+loop_time;                     //total time until reset by Servo output every 20 ms
  
  //PORTB |=(1<< PORTB4);                  //turn on D12 to track loop time 070119 This is not adcEnabled, pin 12 does not track loop time, it just stays high...

  //Reading analog pin for scaling input, without slowing the code, ADC will return a flag when reading is ready  
  if (!adcEnabled) //If the ADC converter is not doing anything, we start taking a reading
  {
    bitSet (ADCSRA, ADSC);             //start a conversion
    adcEnabled = true;
  }
    
  if (bit_is_clear(ADCSRA, ADSC)) //the ADC clears the bit when done
  {
    ADC_VALUE = ADC;  //read result
    adcEnabled = false;  //After we have read the result we set the bit to false because the ADC is turned off again
  }

  //EDGE detection and period measurements
  // start count if D8 goes high


  /*
  reading port B, if pin B0 (D8) is high then turn PinB5 (D13 High)
  In other words, we are checking to see if Pin 8 is high so we can measure the on
  time in order to get the frequency --- it seems like D13 is just an "output" of the input frequency 
  That we are reading...
  */
  if((PINB & (1<<PB0)) >0)
  {
                                    
    if (edge_flag ==0) //write some comments to help the understanding here... 
    {
      elapsed_prev    =elapsed;                 
      On_DC_prev      =On_DC;
      Off_DC_prev     =Off_DC;
      On_DC           =0;
      Off_DC          =0;
      elapsed         =0;          //first time in the loop, rising edge detected
      edge_flag       =1;
     }
     
     //PORTB |=(1<< PORTB5);                 //make pin 13 high and power on the led
     PORTB |=(1<< PORTB4);
     elapse();                             //loop counter for on time 
     ON_DutyCycle();   
  } 
  
  //stop count when D9 goes low
  if((PINB & (1<<PB0)) ==0)
  {                                  //reading port B //if pin B0 (D8) is high then turn PinB5 (D13 High)
    edge_flag = 0;                   //reset for next time edge goes high 
    //PORTB &= ~(1<<PORTB5);           //make pin 13 low and power off the led
    PORTB &= ~(1<<PORTB4);
    elapse();                        //continue loop counter to count entire period
    OFF_DutyCycle();
  }

  engine_speed += (elapsed_prev * loop_time) + periodOffset;
  
  //PORTB &= ~(1<<PORTB4); //D12 low to track loop time
  time1=time0;
  
}
