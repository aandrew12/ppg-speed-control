/*
 * PPG Speed Controller Code
 * 
 * 073019
 * Author: AA
 */

#include <avr/io.h>
#include <util/delay.h>
#include <TimerOne.h>
#include <math.h>

/*
 * Variables for stepper motor controller
 */
 
#define stp 7         
#define dir 6
#define MS1 10
#define MS2 11
#define EN  3

/****************************************/

unsigned long loopCounter   =0;
int stepCounter;                 //do not initialize...
int edge_flag               =0;
int initialOpen             =0;  //number of steps to open for pull start -- used in setup & in main loop
int engineRunning           =0;  //engine running true or false
int delayComplete           =0;  //true if we have finished the current delay step in startup routine
int startupComplete         =0;  //true if we have completed the slow speed up
int startupIndex            =0;  //tells us which value to use from the delay and step arrays during startup...
int timeToDelay;                 //tells us how long to delay at a certain step from the startup routine
int stepsToTake;                 //tells us how many steps to take at a certain step from the startup routine
int stepsTaken              =0;  //steps taken during a given step in the startup routine
int stepStarted             =0;  //used in startup routine to control motor stp pin frequency
int speedControlActive      =0;  //true if we are in speed control mode (startup routine finished)
int enteredStartup          =0;  //tells us if we have entered the startup array
int accelCounter         =3250;  //this value needs to be tweaked to get time scale right

//Trim for period measurement, microseconds added to period measurement
//Set at 100 7/29/2019 on during bench testing
int periodOffset      =100;

unsigned long time0              =0; 
unsigned long time1              =0; 
unsigned long elapsed            =0; 
unsigned long elapsed_prev       =0; 
unsigned long On_DC              =0; 
unsigned long Off_DC             =0; 
unsigned long On_DC_prev         =0; 
unsigned long Off_DC_prev        =0; 
unsigned long out                =0; 
unsigned long Sum                =0;
unsigned long first              =0;
unsigned long loop_time          =0;
unsigned long engine_speed       =0;        //period of the spark frequency in microseconds, 1 spark / revolution 
unsigned long currentSpeed       =0;        //engine speed on current cycle

unsigned long realTimeCounter    =0;        //estimate: incremented by 1 every 4500 cycles, 1 = 0.1s
unsigned long cycleCounter       =0;        //estimate: count 4500 cycles, add 1 to realTimeCounter 

int speedSetPoint       =9230;  //11500;  //9230; //9230=6500, 10720 ~ 5600 RPM
int speedError          =0; 
int stepperDisabled     =0; 
int disabledCounter     =0;
int runAwayCounter      =0;
int runAwayFlag         =0; 
int swungClosed         =0;
int slowOpenCounter     =0;
int swungOpen           =0;
int slowCloseCounter    =0;

//Change this variable to change frequency of step pulses... 50 ~ 490-500 Hz
int loopCounterThreshold = 50; //50 is 500 Hz to stepper motor, lowest allowable value   

float ADC_VALUE         =0;
 
const byte adcPin   =1;  //A1 Analog pin -- this is reserved for if/when we decide to take TPS input into account... 

bool adcEnabled         =false;
bool serial_flag        =false;
bool motorJustMoved     =false;
bool currentlyStepping  =false;

void elapse(){
  elapsed++;
}
void ON_DutyCycle(){
  On_DC++;
}
void OFF_DutyCycle(){
  Off_DC++;
}

void fullCloseThrottle()
{
  digitalWrite(dir, HIGH); //Pull direction pin low to move "forward"
  for(stepCounter= 1; stepCounter<280; stepCounter++)  //Loop the forward stepping enough times for motion to be visible
  {
    digitalWrite(stp,HIGH); //Trigger one step forward
    delayMicroseconds(1);
    digitalWrite(stp,LOW); //Pull step pin low so it can be triggered again
    delay(5);
  }
}

void fullOpenThrottle()
{
  digitalWrite(dir, LOW); //Pull direction pin low to move "forward"
  for(stepCounter= 1; stepCounter<500; stepCounter++)  //Loop the forward stepping enough times for motion to be visible
  {
    digitalWrite(stp,HIGH); //Trigger one step forward
    delayMicroseconds(1);
    digitalWrite(stp,LOW); //Pull step pin low so it can be triggered again
    delay(5);
  }
}

void StepOpenThrottle(int numSteps)
{
  digitalWrite(dir, LOW); //Pull direction pin low to move "forward"
  for(stepCounter= 1; stepCounter<numSteps; stepCounter++)  //Loop the forward stepping enough times for motion to be visible
  {
    digitalWrite(stp,HIGH); //Trigger one step forward
    delayMicroseconds(1);
    digitalWrite(stp,LOW); //Pull step pin low so it can be triggered again
    delay(5);
  }
}

void closeThrottleStart()
{
  digitalWrite(dir, HIGH); //Pull direction pin low to move "forward"
  for(stepCounter= 1; stepCounter<25; stepCounter++)  //Loop the forward stepping enough times for motion to be visible
  {
    digitalWrite(stp,HIGH); //Trigger one step forward
    delayMicroseconds(1);
    digitalWrite(stp,LOW); //Pull step pin low so it can be triggered again
    delay(5);
  }
}

void resetEDPins()
{
  digitalWrite(stp, LOW);
  digitalWrite(dir, LOW);
  digitalWrite(MS1, LOW);
  digitalWrite(MS2, LOW);
  digitalWrite(EN,  LOW);        // low allows motor control
}

void disableStepper() {
  PORTD = PORTD | B00001000;     //digital pin 3 goes high, others remain unchanged from current state
} 

void enableStepper() {
  PORTD = PORTD & B11110111;     //digital pin 3 goes low, others remain unchanged from current state
}

void closeThrottleMode () {
  PORTD = PORTD | B01000000;     
}

void openThrottleMode () {
  PORTD = PORTD & B10111111;
}

void startCurrentStep () {
  PORTD = PORTD | B10000000;
}

void endCurrentStep () {
  PORTD = PORTD & B01111111;
}

void fullStepMode() {
  PORTB = PORTB & B11110011; 
}

void halfStepMode() {
  PORTB = PORTB | B00000100;
  PORTB = PORTB & B11110111; 
}

void quarterStepMode() {
  PORTB = PORTB & B11111011;
  PORTB = PORTB | B00001000; 
}

void eighthStepMode() {
  PORTB = PORTB | B00001100;
}

int delay_array[] =  {50, 100, 150, 190, 230, 238, 244, 250};     //cumlative times at which steps are taken during startup, units of 1/0.1s 
int step_array[]   = {7,  5,   5,   3,   2,   1,   1,   1  };     //number of steps after each delay in startup routine

/*
 * Setup function sets the pin-modes and opens the throttle fully -- this runs whevever the system is power-cycled / reset
 */
void setup(){  

  //Serial.begin(115200);
  
  pinMode(8,    INPUT);  //frequency input (reluctance sensor GEN board)
  pinMode(A1,   INPUT);  //block temp sensor input
  pinMode(9,   OUTPUT);  //troubleshooting
  pinMode(12,  OUTPUT);  //troubleshooting
  pinMode(stp, OUTPUT);  //motor control
  pinMode(dir, OUTPUT);  //motor control
  pinMode(MS1, OUTPUT);  //motor control
  pinMode(MS2, OUTPUT);  //motor control
  pinMode(EN,  OUTPUT);  //motor control

  resetEDPins();                           //reset control pins on stepper motor driver
  fullCloseThrottle();                     //close throttle to begin startup routine....

  int tempADC = analogRead(A1);            //read initial block temp -- use this to set startup schedule...

  int cold_steps =   60;                    //number of steps to take during cold (RT) start (less than RT will always be 10 steps)                    
  int cold_adc   =  147;                    //corresponding cold (RT) start temperature: 147 ADC ~ 22C  
  
  int hot_steps  =  100;                     //number of steps to take during hottest start (block at controlled temp.) 
  int hot_adc    =  287;                    //corresponding hot start temperature: 287 ADC ~ 90C

  initialOpen = map(                       //interpolate the temperature value that we just read into a number of steps
    tempADC,                               //initial block temp       
    cold_adc, hot_adc,                     //adc bounds of block temperature
    cold_steps, hot_steps                  //number of steps to take at upper and lower temperature bounds
  );

  constrain(                               //assure that initialOpen falls within upper and lower bounds
    initialOpen, 
    cold_steps, hot_steps
  );
  
  resetEDPins();                           //reset control pins on stepper motor driver
  
  delay(250);                              //wait a moment once the stepper hits idle screw
  StepOpenThrottle(5);                     //move a step to relieve pressure on idle screw
  delay(250);                              //wait a moment before initial open
  
  StepOpenThrottle(initialOpen);           //open throttle initially for pull start using value calculated above

  double delay_multiplier = map(          //interpolate delay multiplier -- should be twice as fast at hottest temp.
    tempADC,                              //intial block temp
    cold_adc, hot_adc,                    //adc bounds of temperature
    100, 90);                             //multiplier bounds (100 at room temp, 50 at hot start) --> 1.0 and 0.5 *****IF YOU CHANGE THIS

  delay_multiplier = (double) delay_multiplier / 100;    //divide by 100 to get real multiplier (map function above uses integers only)

  constrain(                                             //constrain multipler within upper and lower bounds 
    delay_multiplier,
    0.90, 1.0                                                                                                  //*****YOU MUST CHANGE THIS
  );
  
  accelCounter = delay_multiplier * accelCounter;

} //end of startup sequence (end of setup function), move into speed control mode...


void loop() {                                       

  /*
   * begin startup routine
   */
  
  //Beginning of real time counting...
  //this happens constantly and doesn't stop until the startup routine is complete
  if (startupComplete == 0 && speedControlActive == 0) { 
    cycleCounter++;                                      //Constantly counts the number of entire main loops
    if (cycleCounter > accelCounter) {                   //Every 4500 cycles, increment the realTimeCounter...
      realTimeCounter++;                                 //NOTE: 1 realTimeCounter = 0.1s
      cycleCounter   =0;                                 //Reset cycleCounter to count next 4500 loops... 
    }
  } 
  //End of real time counting...


  //Beginning of engine run detection...
  //Check if the engine is running or not, only if we are not controlling speed and startup routine has not completed
  if (speedControlActive == 0 && startupComplete == 0) { 
    
    if (currentSpeed < 28000 && currentSpeed > 5000) {         //if speed is between (40000 us)1500 and (5000 us)12000 RPM
      engineRunning = 1;                                       //set flag to say engine is running  
    } 
          
    else if (currentSpeed > 42000 || currentSpeed < 2500) {    //detect if the engine dies... (not working at all)
      
        if (engineRunning == 1) {
          engineRunning = 0;                                    //set flag to say that generator is not running
          fullCloseThrottle();                                  //close throttle (RUINS FRQ COUNTING)
          StepOpenThrottle(initialOpen);                        //open throttle to initial point (RUINS FRQ COUNTING)
          delay(100);                                           //stability delay (RUINS FRQ COUNTING)
          currentSpeed    =0;                                   //reset the current speed
          enteredStartup  =0;                                   //set flag, we are no longer in startup mode
          elapsed_prev    =0;                                   //reset counter in frq counter to prevent flase reading...
          loop_time       =0;                                   //reset counter in frq counter to revent false reading...
        }
    }
       
    else {                                                      //for all other cases...
      
      engineRunning = 0;                                        //set flag to say engine is not running                            
      startupComplete = 0;                                      //set flag to say startup is not complete
      startupIndex = 0;                                         //reset the startup array index
      realTimeCounter = 0;                                      //reset the realTimeCounter if engine is not running
      currentSpeed = 0;
    }
  }
  
  //End of engine run detection...


  //realTimeCounter should be active and initialized to 0 when we enter this loop...
  if (engineRunning == 1 && startupComplete == 0) { //if the engine is running AND we haven't done the startup routine, we can begin the startup routine
 
      //now that the engine is running, we need to go through the startup routine...
      if (enteredStartup == 0) {
        realTimeCounter = 0;
        enteredStartup = 1;
      }
    
      timeToDelay = delay_array[startupIndex];           //this is how long to delay before taking steps 
      stepsToTake =  step_array[startupIndex];           //this is the number of steps to take
      
      if (realTimeCounter > timeToDelay) {               //when the realTimeCounter reaches the specified amount, we are done delaying
        delayComplete = 1;
      }

      if (delayComplete == 1) {

        if (stepStarted == 0) {
          fullStepMode();
          enableStepper();
          startCurrentStep();
          stepStarted = 1;
        }
        
        if (stepCounter > 100) {
            endCurrentStep();
            
            if (stepCounter > 200) {
              stepCounter = 0;
              stepStarted = 0;
              stepsTaken++; 
            }          
         }
         
         if (stepsTaken == stepsToTake) {
            delayComplete = 0;
            stepsTaken = 0;
            startupIndex++;
         }
         stepCounter++;
      }

      if (startupIndex > 7) {
        startupComplete = 1;
        loopCounter = 0;
        resetEDPins();
      }     
  }

  /*
   * end of startup routine... 
   */


  /*
   * Start of speed control mode
   */

  loopCounter++;
  if (engineRunning == 1 && startupComplete == 1) {                //start controlling speed only if the engine is running AND startup routine is done

    speedControlActive = 1;
    PORTB |=(1<< PORTB5); // turn on LED to show we are in speed control mode... 
    
    if (loopCounter>loopCounterThreshold && currentlyStepping == false) {
      
      engine_speed = engine_speed / loopCounter / 2; //divide by two to account for step pulse up time and down time
      speedError = engine_speed - speedSetPoint;
      speedError = abs(speedError);
      resetEDPins();
  
      eighthStepMode();                                //unsure if this line is needed
  
      /*Engine speed too slow, open throttle*/
      if (engine_speed > speedSetPoint + 250)            //if engine period is higher than the setpoint, engine is slow, open the throttle
      {
        swungOpen = 0;                             //since speed is low we know we didn't just swing open
        openThrottleMode();
        runAwayFlag = 0;                          //reset the runaway flag/counter, we know the generator is not running too fast
        runAwayCounter = 0;                     
        loopCounterThreshold = 50;  
  
        if (speedError > 2000) {           //if there is a large speed error, swing open
          swungOpen = 1;                   //set the swung open flag because we just reacted quickly
          halfStepMode();
          enableStepper();
          loopCounterThreshold = 50;
        } 
        else {                             //if there error is not large, open slowly
          eighthStepMode();  
          loopCounterThreshold = 100;
          enableStepper();
        }
        
      }
      /*Engine speed too fast, close the throttle*/ 
      else if (engine_speed < speedSetPoint - 250)    //else if the period is lower than the setpoint - deadband, close the throttle
      {
  
        closeThrottleMode();
        
        if (speedError > 2000) {                       //if there is a large error, we still close slowly, runaway is captured elsewhere
          eighthStepMode(); 
          loopCounterThreshold = 150;
          enableStepper();
        }
        else {
          eighthStepMode();                            //catch all -- close slowly TODO: combine this and above into one
          loopCounterThreshold = 150;  
          enableStepper();
        }
      }
      /*engine speed within deadband -- disable motor*/
      else
      {
        disableStepper();                                   //else disable the stepper and set a flag to know we found a good spot
        stepperDisabled = 1;
      }
  
      /*keep motor disabled for a few counts if it caught the deadband*/
      if (stepperDisabled == 1) {                            //if the stepper was recently disabled, keep it disabled for a number of counts
        disableStepper();
        disabledCounter++;
          if (disabledCounter > 75) {                       //adjustment for how long the motor stays disabled 
            disabledCounter = 0;
            stepperDisabled = 0;
          }
      }
  
      if (engine_speed < 7800) {                         //if engine is running very fast (possible brick shut down) set a flag
        runAwayFlag = 1;
        runAwayCounter++;                                //count how many times we saw this high speed               
          if (runAwayCounter > 30) {                     //if we see very high speed for a number of counts, swing closed
            closeThrottleMode();
            halfStepMode();
            enableStepper();
            loopCounterThreshold = 50;
            swungClosed = 1;                              //set a flag to know that we just swung closed
          }
      }
     
      engine_speed = 0;                                   //reset variables get ready to step again....
      startCurrentStep();
      currentlyStepping = true;
      loopCounter =0; 
    }   
    
    else if (loopCounter>loopCounterThreshold && currentlyStepping == true) 
    {
      endCurrentStep();
      currentlyStepping = false;
      loopCounter =0; 
    }
  }

  /*
   * end of speed control mode
   */




   /*
    * start of frequency counter.... 
    */
  
  time0=micros();                        //note there is lag in recycling the loop 
  loop_time=time0-time1;                 //loop time
  out=out+loop_time;                     //total time until reset by Servo output every 20 ms
  
  //PORTB |=(1<< PORTB4);                //turn on D12 to track loop time 070119 This is not adcEnabled, pin 12 does not track loop time, it just stays high...

  //Reading analog pin for scaling input, without slowing the code, ADC will return a flag when reading is ready  
  if (!adcEnabled) //If the ADC converter is not doing anything, we start taking a reading
  {
    bitSet (ADCSRA, ADSC);               //start a conversion
    adcEnabled = true;
  }
    
  if (bit_is_clear(ADCSRA, ADSC)) //the ADC clears the bit when done
  {
    ADC_VALUE = ADC;  //read result
    adcEnabled = false;  //After we have read the result we set the bit to false because the ADC is turned off again
  }

  //EDGE detection and period measurements
  // start count if D8 goes high


  /*
  reading port B, if pin B0 (D8) is high then turn PinB5 (D13 High)
  In other words, we are checking to see if Pin 8 is high so we can measure the on
  time in order to get the frequency --- it seems like D13 is just an "output" of the input frequency 
  That we are reading...
  */
  if((PINB & (1<<PB0)) >0)
  {
                                    
    if (edge_flag ==0) //write some comments to help the understanding here... 
    {
      elapsed_prev    =elapsed;                 
      On_DC_prev      =On_DC;
      Off_DC_prev     =Off_DC;
      On_DC           =0;
      Off_DC          =0;
      elapsed         =0;          //first time in the loop, rising edge detected
      edge_flag       =1;
     }
     
     //PORTB |=(1<< PORTB5);                 //make pin 13 high and power on the led
     PORTB |=(1<< PORTB4);
     elapse();                             //loop counter for on time 
     ON_DutyCycle();   
  } 
  
  //stop count when D9 goes low
  if((PINB & (1<<PB0)) ==0)
  {                                  //reading port B //if pin B0 (D8) is high then turn PinB5 (D13 High)
    edge_flag = 0;                   //reset for next time edge goes high 
    //PORTB &= ~(1<<PORTB5);           //make pin 13 low and power off the led
    PORTB &= ~(1<<PORTB4);
    elapse();                        //continue loop counter to count entire period
    OFF_DutyCycle();
  }

  engine_speed += (elapsed_prev * loop_time) + periodOffset;
  currentSpeed = elapsed_prev * loop_time;
  
  //PORTB &= ~(1<<PORTB4); //D12 low to track loop time
  time1=time0;

  /*
   * end of frequency counter, end of main loop
   */
  
}
