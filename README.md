# PPG Speed Control

This is the PPG Speed Controller Arduino code

This codes reads input frequency and moves a stepper motor in order to keep the frequency at a constant value of 83 Hz.


As of last commit: 1606d5d

	Control scheme changed fundamentally (version 3)

	Motor step pin is constantly being pulsed at roughly 500 Hz which was experimentally determined as its maximum 			speed. 
	
	Disable and direction pins are changed according to engine_speed measurement

	Step size is changed according to magnitiude of speed error: abs(engine_speed - speedSetPoint)

	Deadband in microseconds is a range of speed error where the throttle will not move at all

